#!/usr/bin/python
"""Login to an admin application"""
import unittest

from configuration import test_bed_name as tb
from configuration import driver_name as driver


class LoginApplication(unittest.TestCase):
    """Login to an application"""

    def setUp(self):
        self.browser = driver
        self.browser.get("http://192.168.1.5:8000/admin")
        self.browser.maximize_window()

    def test_login(self):
        """login method to an application"""
        username = self.browser.find_element_by_id(tb.get('username'))
        username.click()
        username.send_keys(tb.get('username_value'))
        password = self.browser.find_element_by_id(tb.get('password'))
        password.click()
        password.send_keys(tb.get('pass_value'))
        submit = self.browser.find_element_by_xpath(tb.get('submit'))
        submit.click()
        link = self.browser.find_element_by_xpath(tb.get('link'))
        link.click()


def suite():
    execute = unittest.TestSuite()
    execute.addTest(LoginApplication('test_login'))
    return execute

if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())