#!/usr/bin/python
"""Login to an admin application"""
import unittest
import os

from selenium import webdriver

DRIVER_PATH = os.path.dirname(os.path.abspath(__file__)) + "/chromedriver"


class Amazon(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome(DRIVER_PATH)
        self.browser.get("https://amazon.in")
        self.browser.maximize_window()

    def test_login(self):
        signin = self.browser.find_element_by_xpath('//*[@id="nav-link-yourAccount"]/span[1]')
        signin.click()
        username = self.browser.find_element_by_xpath('//*[@id="ap_email"]')
        username.click()
        username.send_keys("abc@gmail.com")
        submit = self.browser.find_element_by_xpath('//*[@id="continue"]')
        submit.click()
        password = self.browser.find_element_by_xpath('//*[@id="ap_password"]')
        password.send_keys("admin")
        login = self.browser.find_element_by_xpath('//*[@id="signInSubmit"]')
        login.click()
        redirect = self.browser.find_element_by_xpath('//*[@id="nav-link-yourAccount"]/span[1]')
        logout = self.browser.find_element_by_xpath('//*[@id="nav-item-signout-sa"]/span')
        logout.click()

    def tearDown(self):
        self.browser.close()

if __name__ == "__main__":
    unittest.main()


