#!/usr/bin/python
"""Login to an admin application"""
import time
import unittest
import os

from selenium import webdriver

DRIVER_PATH = os.path.dirname(os.path.abspath(__file__)) + "/chromedriver"


class LoginApplication(unittest.TestCase):
    """Login to an application"""

    def setUp(self):
        self.browser = webdriver.Chrome(DRIVER_PATH)
        self.browser.get("https://www.flipkart.com")
        self.browser.maximize_window()
        time.sleep(3)

    def test_data(self):
        username = self.browser.find_element_by_xpath("/html/body/div[2]/div/div/div/div/div[2]/div/form/div[1]/input")
        username.click()
        username.send_keys("rakesh.ryali@gmail.com")
        password = self.browser.find_element_by_xpath("/html/body/div[2]/div/div/div/div/div[2]/div/form/div[2]/input")
        password.click()
        password.send_keys("aaaa")
        login = self.browser.find_element_by_xpath('/html/body/div[2]/div/div/div/div/div[2]/div/form/div[3]/button/span')
        login.click()
        time.sleep(3)
        profile = self.browser.find_element_by_xpath('//*[@id="container"]/div/header/div[1]/div/div/div/div[2]/div[1]/div/div/div/span/div')
        profile.click()
        logout = self.browser.find_element_by_xpath('//*[@id="container"]/div/header/div[1]/div/div/div/div[2]/div[1]/div/div/div/div/div/div[2]/div/ul/li[7]/a/div')
        logout.click()

    def tearDown(self):
        self.browser.close()

if __name__ == '__main__':
    unittest.main()

