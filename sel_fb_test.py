#!/usr/bin/python
'''fb login and logout with selenium'''

import time
import unittest
from selenium import webdriver

DRIVER_PATH = "C:\Users\user1\PycharmProjects\SkySoft1\chrome_driver\chromedriver"

class LoginApp(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome(DRIVER_PATH)
        self.browser.get("http://www.facebook.com")
        self.browser.maximize_window()
        time.sleep(2)

    def test_login(self):
        username = self.browser.find_element_by_id("email")
        username.click()
        username.send_keys("satishandru143@gmail.com")
        password = self.browser.find_element_by_id("pass")
        password.click()
        password.send_keys("********")
        login = self.browser.find_element_by_xpath('//*[@id="u_0_2"]')
        login.click()
        time.sleep(5)
        label = self.browser.find_element_by_id("userNavigationLabel")
        label.click()
        logout = self.browser.find_element_by_xpath('//*[@id="js_s"]/div/div/ul/li[12]/a/span/span')
        logout.click()
        time.sleep(5)

    def tearDown(self):
        self.browser.close()

def suite():
    execute = unittest.TestSuite()
    execute.addTest(LoginApp('test_login'))
    return execute

if __name__ == "__main__":
    unittest.TextTestRunner(failfast=True).run(suite())


