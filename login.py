#!/usr/bin/python
"""Login to an admin application"""
import time
import unittest
import os
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

DRIVER_PATH = os.path.dirname(os.path.abspath(__file__)) + "/chromedriver"

class LoginApplication(unittest.TestCase):
    """Login to an application"""

    def setUp(self):
        self.browser = webdriver.Chrome(DRIVER_PATH)
        self.browser.get("http://192.168.1.7:8000/admin")
        self.browser.maximize_window()

    def test_login(self):
        """login method to an application"""
        username = self.browser.find_element_by_id("id_username")
        username.click()
        username.send_keys("skysoftadmin")
        password = self.browser.find_element_by_id("id_password")
        password.click()
        password.send_keys("admin")
        submit = self.browser.find_element_by_xpath(
            '//*[@id="login-form"]/div[3]/input')
        submit.click()
        link = self.browser.find_element_by_xpath('//*[@id="user-tools"]/a[1]')
        link.click()

    def test_switch_page(self):
        """Method to swith page and logout"""
        WebDriverWait(self.browser, 5).until(
            EC.presence_of_element_located(
                (By.XPATH, '//*[@id="navbarResponsive"]/ul/li[4]/a')))
        logout = self.browser.find_element_by_xpath('//*[@id="user-tools"]/a[3]')
        logout.click()
        time.sleep(5)

def suite():
    execute = unittest.TestSuite()
    execute.addTest(LoginApplication('test_login'))
    execute.addTest(LoginApplication('test_switch_page'))
    return execute

if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())