#!/usr/bin/python

chrome = {
    'username' : 'id_username',
    'username_value': 'skysoftadmin',
    'password': 'id_password',
    'pass_value': 'admin',
    'submit': '//*[@id="login-form"]/div[3]/input',
    'link': '//*[@id="user-tools"]/a[1]'
}

firefox = {
    'username' : 'id_username',
    'username_value': 'skysoftadmin',
    'password': 'id_password',
    'pass_value': 'admin',
    'submit': '//*[@id="login-form"]/div[3]/input',
    'link': '//*[@id="user-tools"]/a[1]'
}

internet_explorer = {}
opera = {}
safari = {}
